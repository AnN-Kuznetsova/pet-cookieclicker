docker-test: ; sudo DOCKER_BUILDKIT=1 docker build -t clicker-img --target test .
docker-build: ; sudo DOCKER_BUILDKIT=1 docker build -t clicker-img .
docker-run: ; sudo docker run -d -p 8080:80 --rm --name clicker clicker-img
docker-build-run: ; make docker-build && make docker-run
# docker-run-dev: ; sudo docker run -d -p 8080:80 -v "/home/user/AnN/dev/pet-cookieclicker/pet-cookieclicker:/app" -v /app/node_modules --rm --name clicker clicker-img
docker-stop: ; sudo docker stop clicker
