import type { ProductType } from "../types";


export interface RawProductType {
  title: string;
  price: number;
  profit: number;
  imgUrl: string;
  bgUrl: string;
}


const createProduct = (rawProduct: RawProductType, id: string): ProductType => {
  return {
    id,
    title: rawProduct.title,
    price: rawProduct.price,
    profit: rawProduct.profit,
    imgUrl: rawProduct.imgUrl,
    bgUrl: rawProduct.bgUrl,
    count: 0,
    cookiesMined: 0,
  };
};

const createProducts = (rawProducts: RawProductType[]): ProductType[] => {
  return rawProducts.map((rawProduct, index) => createProduct(rawProduct, `${index}`));
};


export {
  createProduct,
  createProducts,
};
