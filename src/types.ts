interface ProductType {
  id: string;
  title: string;
  price: number;
  profit: number;
  imgUrl: string;
  bgUrl: string;
  count: number;
  cookiesMined: number;
}

interface ParametersType {
  top: number;
  right: number;
  bottom: number;
  left: number;
  width: number;
  height: number;
}

interface DropCookieType {
  top: string;
  left: string;
  backgroundPosition: string;
}


export type {
  DropCookieType,
  ProductType,
  ParametersType,
};
