import numeral from "numeral";

import type { ParametersType } from "./types";


numeral.register(`locale`, `cookiesLocale`, {
  delimiters: {
    thousands: ".",
    decimal: ",",
  },
  abbreviations: {
    thousand: " thousand",
    million: " million",
    billion: " billion",
    trillion: " trillion",
  },
  ordinal: function (number) {
    return number === 1 ? "er" : "ème";
  },
  currency: {
    symbol: "cookies",
  },
});

numeral.locale(`cookiesLocale`);

const formatNumber = (num: number): string => {
  return numeral(num).format(`0,0.[000]a`);
};

const getElementsParams = (elem: HTMLElement): ParametersType => {
  const box = elem.getBoundingClientRect();

  return {
    top: box.top + window.pageYOffset,
    right: box.right + window.pageXOffset,
    bottom: box.bottom + window.pageYOffset,
    left: box.left + window.pageXOffset,
    width: box.width,
    height: box.height,
  };
};


export {
  formatNumber,
  getElementsParams,
};
