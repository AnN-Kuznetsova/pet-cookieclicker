import { createSelector } from "@reduxjs/toolkit";

import { productsSelectors } from "./products";
import type { RootStateType } from "../..";
import type { ProductType } from "../../types";


const getProducts = (state: RootStateType): ProductType[] => productsSelectors.selectAll(state) as ProductType[];

const getProductById = (id: string) => (state: RootStateType): ProductType | null =>
  productsSelectors.selectById(state, id) as ProductType || null;

const getProductsCountByTitle = createSelector(
  [
    getProducts,
    (state, title: string) => title,
  ],
  (products, title): number => products.find((product) => product.title.toLowerCase() === title.toLowerCase())?.count || 0,
);

const getProductsTotalProfit = createSelector(
  getProducts,
  (products) => products.reduce((profit, product) => profit + product.count * product.profit, 0) ,
);


export {
  getProducts,
  getProductById,
  getProductsCountByTitle,
  getProductsTotalProfit,
};
