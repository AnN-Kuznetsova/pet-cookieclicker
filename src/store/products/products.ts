import { createEntityAdapter, createSlice, PayloadAction } from "@reduxjs/toolkit";

import { PRICE_INCREASE_PERCENTAGE } from "../../const";
import { createProducts, RawProductType } from "../../adapters/product";
import type { ProductType } from "../../types";
import type { RootStateType } from "../..";


export interface ProductsStateType {
  ids: string[];
  entities: {
    [key: string]: ProductType;
  };
}


export const productsAdapter = createEntityAdapter();
const initialState = productsAdapter.getInitialState() as ProductsStateType;

export const productsSelectors = productsAdapter.getSelectors(
  (state: RootStateType) => state.products
);


const productsStoreSlice = createSlice({
  name: `products`,
  initialState,
  reducers: {
    loadProducts: (state, action: PayloadAction<RawProductType[]>) => {
      productsAdapter.setAll(state, createProducts(action.payload));
    },
    incrementProduct: (state, action: PayloadAction<ProductType>) => {
      const product = action.payload;
      productsAdapter.updateOne(
        state,
        {id: product.id,
          changes:
            {
              count: product.count + 1,
              price: product.price * PRICE_INCREASE_PERCENTAGE,
            }}
      );
    },
    incrementCookiesMined: (state, action: PayloadAction<ProductType[]>) => {
      productsAdapter.updateMany(
        state,
        action.payload.map((product) => ({
          id: product.id,
          changes: {
            cookiesMined: product.cookiesMined + product.count * product.profit,
          },
        }))
      );
    },
  },
});

const {actions, reducer} = productsStoreSlice;


export const {
  loadProducts,
  incrementProduct,
  incrementCookiesMined,
} = actions;

export {
  reducer,
};
