import { configureStore } from "@reduxjs/toolkit";
import { LocalStorageService } from "../storage";

import { reducer as counterReducer } from "./counter/counter";
import { reducer as productsReducer } from "./products/products";


export const createStore = (storage: InstanceType<typeof LocalStorageService>) => {
  return configureStore({
    reducer: {
      counter: counterReducer,
      products: productsReducer,
    },
    devTools: process.env.NODE_ENV !== `production`,
    preloadedState: storage.getStore(),
  });
};
