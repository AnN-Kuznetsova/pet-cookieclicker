import { RootStateType } from "../..";


const getCookiesCount = (state: RootStateType): number => state.counter.totalCoockiesCount;

const getProfitByClick = (state: RootStateType): number => state.counter.profitByClick;

const getCoockiesMinedByClick = (state: RootStateType): number => state.counter.coockiesMinedByClick;


export {
  getCookiesCount,
  getProfitByClick,
  getCoockiesMinedByClick,
};
