import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { PROFIT_BY_CLICK, PROFIT_BY_CLICK_PERCENTAGE } from "../../const";
import { incrementProduct } from "../products/products";
import type { ProductType } from "../../types";


export interface CounterStateType {
  totalCoockiesCount: number;
  coockiesMinedByClick: number;
  profitByClick: number;
}


const initialState: CounterStateType = {
  totalCoockiesCount: 0,
  coockiesMinedByClick: 0,
  profitByClick: PROFIT_BY_CLICK,
};


const counterSlice = createSlice({
  name: `counter`,
  initialState,
  reducers: {
    incrementTotalCoockiesCount: (state) => {
      state.totalCoockiesCount += state.profitByClick;
    },
    incrementCoockiesMinedByClick: (state) => {
      state.coockiesMinedByClick += state.profitByClick;
    },
    changeProfitByClick: (state) => {
      state.profitByClick = PROFIT_BY_CLICK + Math.floor(state.coockiesMinedByClick * PROFIT_BY_CLICK_PERCENTAGE);
    },
    incrementPerTime: (state, action: PayloadAction<number>) => {
      state.totalCoockiesCount += action.payload;
    },
    reset: () => {
      return initialState;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        incrementProduct,
        (state, action: PayloadAction<ProductType>) => {
          state.totalCoockiesCount -= action.payload.price;
        }
      );
  },
});

const {actions, reducer} = counterSlice;


export const {
  incrementTotalCoockiesCount,
  incrementCoockiesMinedByClick,
  changeProfitByClick,
  incrementPerTime,
  reset,
} = actions;

export {
  reducer,
};
