import React, { useEffect, useRef, useState } from "react";

import { COOKIES_TOP_DISPERSION, DROP_COOKIE_COEFFICIENT, MAX_DROP_COOKIES_COUNT } from "../../const";
import { DropCookie } from "../drop-cookie/drop-cookie";
import type { DropCookieType } from "../../types";
import styles from "./drop-cookies.module.scss";


interface PropsType {
  cookiesCountPerSecond: number;
}


const getDropCookieParams = (dropCookiesWrapperWidth = 0, dropCookiesCount = 0) => {
  return {
    top: `${-Math.random() * COOKIES_TOP_DISPERSION * dropCookiesCount}px`,
    left: `${dropCookiesWrapperWidth ? Math.random() * dropCookiesWrapperWidth : 0}px`,
    backgroundPosition: `-${Math.round(Math.random()*12)*41}px -125px`,
  };
};


export const DropCookies: React.FC<PropsType> = (props: PropsType): JSX.Element => {
  const {cookiesCountPerSecond} = props;
  const [currentDrop, setCurrentDrop] = useState(0);
  const [dropCookies, setDropCookies] = useState([] as DropCookieType[]);
  const dropCookiesWrapper = useRef(null);
  let dropCookiesWrapperWidth = 0;

  if (dropCookiesWrapper.current) {
    dropCookiesWrapperWidth = (dropCookiesWrapper.current as HTMLElement).getBoundingClientRect().width;
  }

  useEffect(() => {
    if (cookiesCountPerSecond > currentDrop && dropCookies.length < MAX_DROP_COOKIES_COUNT) {
      setDropCookies((prevDropCookies) => ([...prevDropCookies, getDropCookieParams(dropCookiesWrapperWidth, dropCookies.length)]));
      setCurrentDrop((prevCurrentDrop) => prevCurrentDrop + DROP_COOKIE_COEFFICIENT);
    } else if (cookiesCountPerSecond === 0 && dropCookies.length > 0) {
      setDropCookies([]);
      setCurrentDrop(0);
    }
  }, [cookiesCountPerSecond, currentDrop, dropCookies, dropCookiesWrapperWidth]);

  return (
    <div
      ref={dropCookiesWrapper}
      className={styles.dropCookieWrapper}
    >
      {
        !!dropCookies.length && dropCookies.map((cookie, index) => {
          return dropCookiesWrapperWidth
            ? <DropCookie
              key={`${cookie.top}${cookie.left}${index}`}
              cookie={cookie}
              index={index}
              getDropCookieParams={getDropCookieParams.bind(null, dropCookiesWrapperWidth, dropCookies.length)}
            />
            : null;
        }
        )
      }
    </div>
  );
};
