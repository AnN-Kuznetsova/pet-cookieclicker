import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { CURSOR_PADDING, CURSOR_ROTATE_DEG } from "../../const";
import { changeProfitByClick, incrementCoockiesMinedByClick, incrementTotalCoockiesCount } from "../../store/counter/counter";
import { getProductsCountByTitle } from "../../store/products/selectors";
import { getProfitByClick } from "../../store/counter/selectors";
import type { RootStateType } from "../..";
import styles from "./big-cookie-button.module.scss";


const createCookieNumbersElement = (cookieNumbers: number, key: string, onAnimationEnd: () => void): JSX.Element => {
  const handleAnimationEnd = () => {
    onAnimationEnd();
  };

  return (
    <span
      key={key}
      className={styles.cookieNumbers}
      onAnimationEnd={handleAnimationEnd}
    >
        +{cookieNumbers}
    </span>
  );
};


export const BigCookieButton: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const cursorsCount = useSelector((state: RootStateType) => getProductsCountByTitle(state, `cursor`));
  const cookieNumbers = useSelector(getProfitByClick);

  const [clickCount, setClickCount] = useState(0);
  const [cookieNumbersElements, setCookieNumbersElements] = useState<JSX.Element[]>([]);

  const getCookieNumbersElements = useRef(() => cookieNumbersElements);

  useEffect(() => {
    getCookieNumbersElements.current = () => cookieNumbersElements;
  });

  const onAnimationEnd = (key: string) => {
    const cookieNumbersElements = getCookieNumbersElements.current();
    setCookieNumbersElements(cookieNumbersElements.filter((element) => element.key !== key));
  };

  const addCookieNumbersElement = (): void => {
    const key = `${clickCount}${cookieNumbers}`;
    const cookieNumbersElement = createCookieNumbersElement(cookieNumbers, key, onAnimationEnd.bind(null, key));
    setCookieNumbersElements((prevElements) => ([...prevElements, cookieNumbersElement]));
  };

  const handleButtonClick = () => {
    dispatch(incrementTotalCoockiesCount());
    dispatch(incrementCoockiesMinedByClick());
    dispatch(changeProfitByClick());

    setClickCount(clickCount + 1);
    addCookieNumbersElement();
  };

  return (
    <div className={styles.bigCookieButtonWrapper}>
      <div className={styles.cursorsCircle}>
        {
          new Array(cursorsCount).fill(true).map((cursorItem, index) => {
            const angle = CURSOR_ROTATE_DEG * index + index;
            const line = Math.floor(angle / 360);

            return (
              <div
                key={cursorItem + index}
                className={styles.arc}
                style={{
                  paddingBottom: `${CURSOR_PADDING + line * 7}%`,
                  transform: `rotate(-${angle}deg)`,
                }}
              >
                <span className={styles.cursor}>Cursor</span>
              </div>
            );
          })
        }
      </div>

      <button
        className={styles.bigCookieButton}
        onClick={handleButtonClick}
      >
        Big
        <br/>
        Cookie
      </button>

      {!!cookieNumbersElements.length && cookieNumbersElements.map((element) => element)}
    </div>
  );
};
