import React, { useRef, useState } from "react";
import classNames from "classnames";
import { useDispatch, useSelector } from "react-redux";

import { ProductCardMargin } from "../../const";
import { formatNumber } from "../../utils";
import { getCookiesCount } from "../../store/counter/selectors";
import { getElementsParams } from "../../utils";
import { incrementProduct } from "../../store/products/products";
import { ProductCard } from "../product-card/product-card";
import type { ProductType } from "../../types";
import styles from "./product-item.module.scss";


export interface PropsTypes {
  product: ProductType;
  isUnknown: boolean;
}


const useProductCard = (
  product: ProductType,
  productElement: HTMLElement | null
): JSX.Element | null => {
  const productCardCoordinates = {
    top: 0,
    right: 0,
  };

  if (productElement) {
    const productElementParams = getElementsParams(productElement);

    productCardCoordinates.right = productElementParams.right - productElementParams.left + ProductCardMargin.LEFT;
    productCardCoordinates.top = productElementParams.top;
  }

  return productElement
    ? <ProductCard
      product={product}
      coordinates={productCardCoordinates}
    />
    : null;
};


export const ProductItem: React.FC<PropsTypes> = (props: PropsTypes): JSX.Element => {
  const {
    product,
    isUnknown,
  } = props;

  const dispatch = useDispatch();
  const cookiesCount = useSelector(getCookiesCount);
  const [isProductCardVisible, setIsProductCardVisible] = useState(false);
  const productElement = useRef(null);
  const productCard = useProductCard(product, productElement.current);

  const wrapperClassName = classNames({
    [`${styles.itemWrapper}`]: true,
    [`${styles.isUnknown}`]: isUnknown,
  });
  const buttonClassName = classNames({
    [`${styles.item}`]: true,
    [`${styles.disabled}`]: cookiesCount < product.price,
  });

  const hanleProductButtonClick = (product: ProductType) => {
    dispatch(incrementProduct(product));
  };

  const handleItemMouseEnter = () => {
    setIsProductCardVisible(true);
  };

  const handleItemMouseLeave = () => {
    setIsProductCardVisible(false);
  };

  return (
    <li
      ref={productElement}
      className={wrapperClassName}
      onMouseEnter={handleItemMouseEnter}
      onMouseLeave={handleItemMouseLeave}
    >
      <button
        type="button"
        className={buttonClassName}
        style={{
          backgroundImage: `${isUnknown ? `none` : `url(${`.` + product.imgUrl})`}`,
        }}
        onClick={hanleProductButtonClick.bind(null, product)}
      >
        <span className={styles.info}>
          <span>{isUnknown && `???` || product.title}</span>
          <span className={styles.price}>{formatNumber(product.price)}</span>
        </span>

        {isUnknown || <span className={styles.count}>{product.count}</span>}
      </button>

      {isProductCardVisible && productCard}
    </li>
  );
};
