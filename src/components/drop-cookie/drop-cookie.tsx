import React, { useMemo, useState } from "react";

import type { DropCookieType } from "../../types";
import styles from "./drop-cookie.module.scss";


interface PropsType {
  cookie: DropCookieType;
  index: number;
  getDropCookieParams: ()=>DropCookieType;
}


export const DropCookie: React.FC<PropsType> = (props: PropsType): JSX.Element => {
  const {
    cookie,
    index,
    getDropCookieParams,
  } = props;

  const [cookieParams, setCookieParams] = useState(cookie);
  const animationDelay = useMemo(() => `${Math.round(Math.random() * index)}s`, [index]);

  const handleAnimationIteration = () => {
    setCookieParams(getDropCookieParams());
  };

  return (
    <span
      className={styles.dropCookie}
      style={{
        left: cookieParams.left,
        top: cookieParams.top,
        backgroundPosition: cookieParams.backgroundPosition,
        animationDelay: `${animationDelay}`,
      }}
      onAnimationIteration={handleAnimationIteration}
    ></span>
  );
};
