import * as React from "react";
import { render, screen } from "@testing-library/react";

import { Counter } from "./counter";


describe(`Counter`, () => {
  it(`Should be correct render 15/255`, () => {
    render(
      <Counter
        cookiesCount={15}
        cookiesPerSecond={255}
      />
    );

    expect(screen.getByText(/15 cookies/i)).toBeInTheDocument();
    expect(screen.getByText(/per second: 255/i)).toBeInTheDocument();
  });

  it(`Should be correct render with "thousand"`, () => {
    render(
      <Counter
        cookiesCount={32145}
        cookiesPerSecond={168550}
      />
    );

    expect(screen.getByText(/32,145 thousand cookies/i)).toBeInTheDocument();
    expect(screen.getByText(/per second: 168,55 thousand/i)).toBeInTheDocument();
  });

  it(`Should be correct render with "million"`, () => {
    render(
      <Counter
        cookiesCount={3214565}
        cookiesPerSecond={164858550}
      />
    );

    expect(screen.getByText(/3,215 million cookies/i)).toBeInTheDocument();
    expect(screen.getByText(/per second: 164,859 million/i)).toBeInTheDocument();
  });

  it(`Should be correct render with "million"`, () => {
    render(
      <Counter
        cookiesCount={3214565}
        cookiesPerSecond={164858550}
      />
    );

    expect(screen.getByText(/3,215 million cookies/i)).toBeInTheDocument();
    expect(screen.getByText(/per second: 164,859 million/i)).toBeInTheDocument();
  });
});
