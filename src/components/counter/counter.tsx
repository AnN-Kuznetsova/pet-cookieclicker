import React from "react";

import { formatNumber } from "../../utils";
import styles from "./counter.module.scss";


interface PropsType {
  cookiesCount: number;
  cookiesPerSecond: number;
  className?: string;
}


export const Counter: React.FC<PropsType> = (props: PropsType): JSX.Element => {
  const {
    cookiesCount,
    cookiesPerSecond,
    className,
  } = props;

  return (
    <div className={`${styles.counter} ${className}`}>
      <h1>{formatNumber(cookiesCount)} cookies</h1>
      <p>per second: {formatNumber(cookiesPerSecond)}</p>
    </div>
  );
};
