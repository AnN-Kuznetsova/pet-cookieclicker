import React from "react";
import { useSelector } from "react-redux";

import { MIN_PRODUCTS_ITEM_COUNT } from "../../const";
import { ProductItem } from "../product-item/product-item";
import { getProducts } from "../../store/products/selectors";
import styles from "./store.module.scss";


export const Store: React.FC = (): JSX.Element => {
  const products = useSelector(getProducts);
  const visibleProducts = products.filter((product, index, productsArr) => index <= 1 || productsArr[index - 2].count >= MIN_PRODUCTS_ITEM_COUNT);

  return (
    <>
      <h2>Store</h2>

      <ul className={styles.productsList}>
        {visibleProducts.map((product, index) => {
          const isUnknown = index === visibleProducts.length - 1 &&  visibleProducts[index - 1].count < MIN_PRODUCTS_ITEM_COUNT;

          return (
            <ProductItem
              key={`${product.title + index}`}
              product={product}
              isUnknown={isUnknown}
            />
          );
        })}
      </ul>
    </>
  );
};
