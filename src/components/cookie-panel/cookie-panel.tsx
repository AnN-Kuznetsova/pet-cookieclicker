import React from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";

import { COOKIES_COUNR_FOR_SHOWER } from "../../const";
import { BigCookieButton } from "../big-cookie-button/big-cookie-button";
import { Counter } from "../counter/counter";
import { getCookiesCount } from "../../store/counter/selectors";
import { getProductsTotalProfit } from "../../store/products/selectors";
import styles from "./cookie-panel.module.scss";
import { DropCookies } from "../drop-cookies/drop-cookies";
import { CookieShower } from "../cookie-shower/cookie-shower";


const StyledCounter = styled(Counter)`
  margin: 100px 0;
  z-index: 10;
`;


export const CookiePanel: React.FC = (): JSX.Element => {
  const cookiesCount = useSelector(getCookiesCount);
  const cookiesCountPerSecond = useSelector(getProductsTotalProfit);
  const isCookieShower = cookiesCountPerSecond > COOKIES_COUNR_FOR_SHOWER;

  return (
    <div className={styles.cookiePanelWrapper}>
      {isCookieShower && <CookieShower />}
      <DropCookies cookiesCountPerSecond={cookiesCountPerSecond} />

      <StyledCounter
        cookiesCount={cookiesCount}
        cookiesPerSecond={cookiesCountPerSecond}
      />
      <BigCookieButton />
    </div>
  );
};
