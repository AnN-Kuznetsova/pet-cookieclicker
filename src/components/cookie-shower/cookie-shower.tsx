import React from "react";

import styles from "./cookie-shower.module.scss";


export const CookieShower: React.FC = (): JSX.Element => {
  return (
    <div className={styles.cookieShower}>
      <div className={styles.cookieShowerInner}></div>
    </div>
  );
};
