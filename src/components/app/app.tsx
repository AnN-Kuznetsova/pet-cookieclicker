import * as React from "react";
import { useDispatch } from "react-redux";

import { RestartButton } from "../restart-button/restart-button";
import { SectionPanels } from "../section-panels/section-panels";
import { initiateProducts } from "../..";
import { reset as resetCounter } from "../../store/counter/counter";
import "./app.module.scss";


export const App: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();

  const handleRestartButtonClick = () => {
    initiateProducts();
    dispatch(resetCounter());
  };


  return (
    <>
      <main>
        <SectionPanels />
      </main>
      <RestartButton onClick={handleRestartButtonClick} />
    </>
  );
};
