import React, { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import classNames from "classnames";
import { useSelector } from "react-redux";

import { ProductCardMargin } from "../../const";
import { getElementsParams } from "../../utils";
import { getProductsTotalProfit } from "../../store/products/selectors";
import { formatNumber } from "../../utils";
import type { ProductType } from "../../types";
import styles from "./product-card.module.scss";


interface PropsType {
  product: ProductType;
  coordinates: {
    top: number;
    right: number;
  }
}


const rootElement = document.querySelector(`#root`) as Element;


export const ProductCard: React.FC<PropsType> = (props: PropsType): JSX.Element => {
  const {
    product,
    coordinates,
  } = props;

  const productCardElementRef = useRef(null);
  const [productCardCoordinates, setProductCardCoordinates] = useState(coordinates);
  const rootElementCoordinates = getElementsParams(rootElement as HTMLElement);

  const titleClassName = classNames({
    [`${styles.title}`]: true,
    [`${styles[`title--${product.title.replaceAll(` `, `-`).toLowerCase()}`]}`]: true,
  });

  const productProfitPerTime = product.count * product.profit;
  const totalProfitPerTime = useSelector(getProductsTotalProfit);
  const percent = totalProfitPerTime ? productProfitPerTime / totalProfitPerTime * 100 : 0;

  useEffect(() => {
    if (productCardElementRef.current) {
      const productCardElementParams = getElementsParams(productCardElementRef.current as HTMLElement);

      if (productCardElementParams.top < rootElementCoordinates.top) {
        setProductCardCoordinates((state) => ({
          right: state.right,
          top: ProductCardMargin.TOP,
        }));
      }

      if (productCardElementParams.bottom > rootElementCoordinates.bottom) {
        setProductCardCoordinates((state) => ({
          right: state.right,
          top: rootElementCoordinates.bottom - productCardElementParams.height - ProductCardMargin.TOP,
        }));
      }
    }
  }, [setProductCardCoordinates, rootElementCoordinates]);

  return ReactDOM.createPortal(
    <article
      ref={productCardElementRef}
      className={styles.productCard}
      style={{
        top: `${productCardCoordinates.top}px`,
        right: `${productCardCoordinates.right}px`,
      }}
    >
      <h6 className={titleClassName}>{product.title}</h6>
      <ul className={styles.features}>
        <li className={styles.featureItem}>
            each {product.title} produces <b>{formatNumber(product.profit)}</b> cookies per second
        </li>
        <li className={styles.featureItem}>
          {product.count} {product.title}s producing <b>{formatNumber(productProfitPerTime)}</b> cookies per second
            (<b>{formatNumber(percent)}%</b> of total CpS)
        </li>
        <li className={styles.featureItem}>
          <b>{formatNumber(product.cookiesMined)}</b> cookies produced so far
        </li>
      </ul>
    </article>,
    rootElement
  );
};
