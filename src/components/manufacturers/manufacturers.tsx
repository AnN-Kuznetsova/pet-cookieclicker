import React from "react";
import { useSelector } from "react-redux";

import { getProducts } from "../../store/products/selectors";
import styles from "./manufacturers.module.scss";


export const Manufacturers: React.FC = (): JSX.Element => {
  const products = useSelector(getProducts)
    .filter((propduct) => propduct.count > 0);

  return (
    <>
      <ul className={styles.list}>
        {
          products.map((product) => {
            return product.title.toLowerCase() === `cursor` ? null : (
              <li
                key={`${product.title + product.id}`}
                className={styles.item}
                style={{backgroundImage: `url(${`.` + product.bgUrl})`}}
              >
                {
                  new Array(product.count).fill(true).map((item, index) => (
                    <span
                      key={`${product.title + index}`}
                      className={`${styles.itemImg} ${styles[`itemImg--${product.title}`]}`}
                      style={{backgroundImage: `url(${`.` + product.imgUrl})`}}
                    >
                    </span>
                  ))
                }
              </li>
            );
          })
        }
      </ul>
    </>
  );
};
