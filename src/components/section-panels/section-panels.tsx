import React, { ReactElement } from "react";
import classNames from "classnames";

import { CookiePanel } from "../cookie-panel/cookie-panel";
import { Manufacturers } from "../manufacturers/manufacturers";
import { Store } from "../store/store";
import styles from "./section-panels.module.scss";


const createPanel = (element: ReactElement, panelClassName?: string) => {
  const className = classNames({
    [`${styles.panel}`]: true,
    [`${styles[`panel--${panelClassName}`]}`]: !!panelClassName,
  });

  return (
    <section className={className}>
      {element}
    </section>
  );
};


export const SectionPanels: React.FC = (): JSX.Element => {
  return (
    <div className={styles.sectionPanels}>
      {createPanel(<CookiePanel />, `cookie`)}
      {createPanel(<Manufacturers />, `manufacturers`)}
      {createPanel(<Store />, `store`)}
    </div>
  );
};
