import React from "react";

import styles from "./restart-button.module.scss";


interface PropsType {
  onClick: ()=>void;
}


export const RestartButton: React.FC<PropsType> = (props: PropsType): JSX.Element => {
  const {onClick} = props;

  return (
    <button
      type="button"
      className={styles.restartButton}
      onClick={onClick}
    >
      Restart
    </button>
  );
};
