import { RootStateType } from ".";


const KEY = `store`;


export class LocalStorageService {
  private _storage = window.sessionStorage;
  private _store: {getState: () => RootStateType} | null = null;

  initiateStore(store: {getState: () => RootStateType}) {
    this._store = store;
  }

  getStore() {
    try {
      const serializedStore = this._storage.getItem(KEY);
      if (!serializedStore) {
        return undefined;
      }
      return JSON.parse(serializedStore);
    } catch (error) {
      return undefined;
    }
  }

  setStore() {
    if (this._store) {
      try {
        const serializedStore = JSON.stringify(this._store.getState());
        this._storage.setItem(KEY, serializedStore);
      } catch (error) {
        // Ignore
      }
    }
  }
}
