import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import rawProducts from "./data/products.json";
import { App } from "./components/app/app";
import { LocalStorageService } from "./storage";
import { TICK_TIME } from "./const";
import { createStore } from "./store/store";
import { getProducts, getProductsTotalProfit } from "./store/products/selectors";
import { incrementPerTime } from "./store/counter/counter";
import { incrementCookiesMined, loadProducts } from "./store/products/products";
import "./css/index.module.scss";


const storage = new LocalStorageService();
const store = createStore(storage);
storage.initiateStore(store);


export const initiateProducts = () => {
  store.dispatch(loadProducts(rawProducts));
};

if (!storage.getStore()) {
  initiateProducts();
}


const renderDom = (): void => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.querySelector(`#root`)
  );
};

renderDom();


setInterval(() => {
  const profitPerTime = getProductsTotalProfit(store.getState());
  store.dispatch(incrementPerTime(profitPerTime));
  store.dispatch(incrementCookiesMined(getProducts(store.getState())));

  storage.setStore();
}, TICK_TIME);


export type RootStateType = ReturnType<typeof store.getState>;
export type AppDispatchType = typeof store.dispatch;
