- Этот файл здесь не нужен
- Всё содержимое должно быть в ".git/hooks/pre-commit" (без расширения)

---------------------------------------------------
*Только ESLint*
---------------------------------------------------

#!/bin/bash
cd "$(git rev-parse --show-toplevel)"
ESLINT="node_modules/.bin/eslint"
pwd

if [[ ! -x "$ESLINT" ]]; then
  printf "\t\033[41mPlease install ESlint\033[0m (npm install eslint)\n"
  exit 1
fi

STAGED_FILES=($(git diff --cached --name-only --diff-filter=ACM | grep ".tsx\{1\}\|.ts\{1\}$"))

echo "ESLint'ing ${#STAGED_FILES[@]} files"

if [[ "$STAGED_FILES" = "" ]]; then
  exit 0
fi

$ESLINT "${STAGED_FILES[@]}" --fix

ESLINT_EXIT="$?"

# Re-add files since they may have been fixed
git add "${STAGED_FILES[@]}"

if [[ "${ESLINT_EXIT}" == 0 ]]; then
  printf "\n\033[42mCOMMIT SUCCEEDED\033[0m\n"
else
  printf "\n\033[41mCOMMIT FAILED:\033[0m Fix eslint errors and try again\n"
  exit 1
fi

exit $?


--------------------------------------------------------
*ESLint + StyleLint (хз насколько корректно работает)*
--------------------------------------------------------

#!/bin/bash
cd "$(git rev-parse --show-toplevel)"
ESLINT="node_modules/.bin/eslint"
STYLELINT="node_modules/.bin/stylelint"
pwd

if [[ ! -x "$ESLINT" ]]; then
  printf "\t\033[41mPlease install ESlint\033[0m (npm install eslint)\n"
  exit 1
fi

if [[ ! -x "$STYLELINT" ]]; then
  printf "\t\033[41mPlease install StyleLint\033[0m (npm install stylelint stylelint-config-standard)\n"
  exit 1
fi

STAGED_FILES_ES=($(git diff --cached --name-only --diff-filter=ACM | grep ".tsx\{1\}\|.ts\{1\}$"))
STAGED_FILES_STYLE=($(git diff --cached --name-only --diff-filter=ACM | grep ".css\{1\}\|.scss\{1\}$"))


if [[ "$STAGED_FILES_ES" = "" && "$STAGED_FILES_STYLE" = ""  ]]; then
  echo "ESLint'ing ${#STAGED_FILES_ES[@]} files"
  echo "StyleLint'ing ${#STAGED_FILES_STYLE[@]} files"
  exit 0
fi


echo "ESLint'ing ${#STAGED_FILES_ES[@]} files"
if [[ "$STAGED_FILES_ES" != "" ]]; then
  $ESLINT "${STAGED_FILES_ES[@]}" --fix
fi
ESLINT_EXIT="$?"


echo "StyleLint'ing ${#STAGED_FILES_STYLE[@]} files"
if [[ "$STAGED_FILES_STYLE" != ""  ]]; then
  $STYLELINT "${STAGED_FILES_STYLE[@]}" --fix
fi
STYLELINT_EXIT="$?"


# Re-add files since they may have been fixed
git add .


if [[ "${ESLINT_EXIT}" == 0 && "${STYLELINT_EXIT}" == 0 ]]; then
  printf "\n\033[42mCOMMIT SUCCEEDED\033[0m\n"
else
  printf "\n\033[41mCOMMIT FAILED:\033[0m Fix errors and try again\n"
  exit 1
fi

exit $?
